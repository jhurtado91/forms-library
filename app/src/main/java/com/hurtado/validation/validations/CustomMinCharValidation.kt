package com.hurtado.validation.validations

import com.hurtado.forms.validations.MinCharValidation

class CustomMinCharValidation: MinCharValidation(3)